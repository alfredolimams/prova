package iResearcher;

import java.util.GregorianCalendar;

public class Message {
	
	private String message;
	private GregorianCalendar date;
	public Message( String message ) {
		setMessage(message);
		setDate( new GregorianCalendar() );
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public GregorianCalendar getDate() {
		return date;
	}
	public void setDate(GregorianCalendar date) {
		this.date = date;
	}
}
