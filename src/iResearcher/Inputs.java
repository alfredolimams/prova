package iResearcher;

import java.util.Scanner;

public class Inputs {

	private Scanner input;

	public Inputs(Scanner input) {
		setInput(input);
	}

	public int nextInt() {
		int value = 0;
		try {
			String number = input.nextLine();
			value = Integer.parseInt(number);
		} catch (NumberFormatException exception) {
			System.out.println("Somente inteiro, por favor");
		}
		System.out.println("Opção: " + value);
		return value;
	}

	public String nextLine(String request) {
		System.out.print(request + ": ");
		return input.nextLine();
	}
	public String next( String request ){
		System.out.print(request + ": ");
		return input.next();
	}

	public Scanner getInput() {
		return input;
	}

	public void setInput(Scanner input) {
		this.input = input;
	}

}
