package iResearcher;

import java.util.HashMap;
import java.util.Map;

public class Manager {

	private Map<String, Users> users;
	private Map<String, Community> communitys;

	public Manager() {
		users = new HashMap<String, Users>();
		communitys = new HashMap<String, Community>();
	}

	public boolean addUser(Users user) {

		if (users.containsKey(user.getNick())) {
			System.out.println("Nick já usado!");
			return false;
		}
		users.put(user.getNick(), user);
		return true;
	}

	public boolean addCommunity(Community community) {

		if (communitys.containsKey(community.getName())) {
			System.out.println("Comunida já criada");
			return false;
		}
		communitys.put(community.getName(), community);
		return true;
	}

	public Users searchUser(String nick) {

		if (users.containsKey(nick)) {
			return users.get(nick);
		}
		return null;
	}
	public Community searchCommunity(String name) {

		if (communitys.containsKey(name)) {
			return communitys.get(name);
		}
		return null;
	}
	public void removeUser( String nick ) {
		users.remove(nick);
	}
}
