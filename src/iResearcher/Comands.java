package iResearcher;

import java.util.LinkedList;
import java.util.Scanner;

public class Comands {

	private Scanner input;
	private Inputs inputs;
	private Manager manager;
	Users user;

	public Comands(Scanner input) {
		setInput(input);
		inputs = new Inputs(input);
		manager = new Manager();
		user = null;
	}

	public void menuMain() {
		System.out.println(" 1 - Criar Conta , 2 - Edição de Perfil ");
		System.out.println(" 3 - Adição de Amigos , 4 - Enviar Mensagem ");
		System.out.println(" 5 - Criar Comunidade , 6 - Entrar em uma Comunidade ");
		System.out.println(" 7 - Informações , 8 - Remover Conta ");
		System.out.println(" 9 - Login , 10 - Logout ");
	}

	private void menuEdit() {
		System.out.println(" 1 - Nome , 2 - Senha ");
		System.out.println(" 3 - Idade , 2 - Senha ");
	}

	private void menuLogin() {
		System.out.println(" Solicitações de Amizades ");
		System.out.println(" 1 - Sim , 2 - Não ");
	}

	public void read() {
		int option = inputs.nextInt();
		if (validComand(1, option, 10)) {
			comand(option);
		} else {
			System.out.println("Opção inválido");
			return;
		}
	}

	public void comand(int option) {

		if (option == 1 && !on()) {
			createAccount();
		} else if (option == 2 && on()) {
			editAccount();
		} else if (option == 3 && on()) {
			addFriend();
		} else if (option == 4 && on()) {
			sendMessage();
		} else if (option == 5 && on()) {
			createCommunity();
		} else if (option == 6 && on()) {
			associateCommunity();
		} else if (option == 7 && on()) {
			System.out.println(user);
		} else if (option == 8 && on()) {
			removeAccount();
		} else if (option == 9 && !on()) {
			login();
		} else if (option == 10 && on()) {
			logout(true);
		} else {
			if (on()) {
				System.out.println("Você estar logado");
			} else {
				System.out.println("Você não estar logado");
			}
		}

	}

	public Scanner getInput() {
		return input;
	}

	public void setInput(Scanner input) {
		this.input = input;
	}

	private boolean validComand(int lower, int comand, int upper) {
		return lower <= comand && comand <= upper;
	}

	private boolean validNewNick(String nick) {
		return manager.searchUser(nick) == null;
	}

	private boolean on() {
		return user != null;
	}

	private void createAccount() {
		String name = inputs.nextLine("Nome");
		System.out.print("Idade: ");
		int age = inputs.nextInt();
		boolean flag = false;
		String nick;
		do {
			nick = inputs.nextLine("Nick");
			flag = validNewNick(nick);
			if (!flag) {
				System.out.println("Nick inválido!");
			}
		} while (!flag);
		String password = inputs.nextLine("Senha");
		manager.addUser(new Users(nick, password, name, age));
	}

	private void editAccount() {
		menuEdit();
		int option = inputs.nextInt();
		if (validComand(1, option, 3)) {
			setUser(option);
		} else {
			System.out.println("Opção inválido");
			return;
		}

	}

	private void logout(boolean print) {
		user = null;
		if (print) {
			System.out.println("Logout com sucesso!");
		}
	}

	private void login() {
		String nick = inputs.nextLine("Nick");
		String password = inputs.nextLine("Senha");
		user = manager.searchUser(nick);
		if (user != null && user.checkPassword(password)) {
			System.out.println("Login com Sucesso!");
			menuLogin();
			request();
		} else {
			System.out.println("Login mal sucedido!");
			logout(false);
		}
	}

	private void setUser(int option) {
		if (option == 1) {
			String name = inputs.nextLine("Novo nome");
			user.setName(name);
		} else if (option == 2) {
			String password = inputs.nextLine("Nova senha");
			user.setPassword(password);
		} else {
			int age = inputs.nextInt();
			user.setAge(age);
		}
	}

	private void createCommunity() {
		String name = inputs.nextLine("Nome da Comunidade");
		String description = inputs.nextLine("Descrição");
		if (manager.searchCommunity(name) == null) {
			manager.addCommunity(new Community(name, description));
			System.out.println("Comunidade criada com sucesso!");
		} else {
			System.out.println("Comunidade já criada!");
		}
	}

	private void addFriend() {
		String nick = inputs.nextLine("Nick do amigo");
		Users friend = manager.searchUser(nick);
		if (friend == null) {
			System.out.println("Amigo não encontrado");
		} else {
			friend.addRequest(user.getNick());
			System.out.println("Solicitação enviada");
		}
	}

	private void sendMessage() {
		String nick = inputs.nextLine("Nick do amigo");
		Users friend = manager.searchUser(nick);
		if (friend == null) {
			System.out.println("Usuário não encontrado");
		} else {
			String message = inputs.nextLine("Mensagem");
			Message messages = new Message(message);
			friend.addMessage(user.getNick(), messages);
			user.addMessage(nick, messages);
			System.out.println("Mensagem enviada");
		}
	}

	private void associateCommunity() {
		String name = inputs.nextLine("Nome da comunidade");
		Community community = manager.searchCommunity(name);
		if (community == null) {
			System.out.println("Nenhuma comunidade com este nome");
		} else {
			user.addCommunity(name);
			community.addAssociate(user.getNick());
		}
	}

	private void removeAccount() {
		for (String key : user.getMessages().keySet()) {
			manager.searchUser(key).removeUser(user.getNick());
		}
		for (String key : user.getFriends()) {
			manager.searchUser(key).removeUser(user.getNick());
		}
		for (String key : user.getCommunitys()) {
			manager.searchCommunity(key).removeAssociate(user.getNick());
		}
		manager.removeUser(user.getNick());
		logout(false);
	}

	private void request() {
		LinkedList<String> friends = user.getRequest();
		while (!friends.isEmpty()) {
			System.out.println("Aceitar a amizade de " + friends.getFirst());
			int option = inputs.nextInt();
			if (option != 1 || option != 2) {
				System.out.println("Opção inválido");
			} else {
				String nick = friends.removeFirst();
				if (option == 1) {
					user.addFriend(nick);
				}
			}
		}
	}

}
