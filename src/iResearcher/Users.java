package iResearcher;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Users extends People {

	private String nick;
	private String password;
	private Map< String, LinkedList< Message > > messages;
	private Set< String > communitys;
	private Set< String > friends;
	LinkedList<String> request;
	public Set<String> getFriends() {
		return friends;
	}
	public void setFriends(Set<String> friends) {
		this.friends = friends;
	}
	public Users( String nick, String password, String name, int age  ) {
		super(name, age);
		setNick(nick);
		setPassword(password);
		messages = new HashMap< String, LinkedList< Message > >();
		request = new LinkedList<String>();
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Map<String, LinkedList<Message>> getMessages() {
		return messages;
	}
	public void setMessages(Map<String, LinkedList<Message>> messages) {
		this.messages = messages;
	}
	public LinkedList<String> getRequest() {
		return request;
	}
	public void setRequest(LinkedList<String> request) {
		this.request = request;
	}
	public void addMessage(String nick , Message message ) {
		if( !messages.containsKey(nick) ) {
			messages.put(nick, new LinkedList< Message >() );
		}
		LinkedList<Message> list = messages.get(nick);
		list.addLast(message);
	}
	public boolean checkPassword( String password ) {
		return this.password.compareTo(password) == 0;
	}
	public void addRequest( String nick ) {
		request.addLast(nick);
	}
	public Set<String> getCommunitys() {
		return communitys;
	}
	public void setCommunitys(Set<String> communitys) {
		this.communitys = communitys;
	}
	public void addCommunity( String community ) {
		communitys.add(community);
	}
	public void addFriend( String nick ) {
		friends.add(nick);
	}
	public void removeUser( String nick ) {
		friends.remove(nick);
		messages.remove(nick);
		request.remove(nick);
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + allCommunitys() + allFriends() + allMessage();
	}
	
	private String allCommunitys() {
		String list = "Comunidades: \n";
		for( String key : communitys ) {
			list += key + "\n";
		}
		return list;
	}
	private String allFriends() {
		String list = "Amigos: \n";
		for( String key : friends ) {
			list += key + "\n";
		}
		return list;
	}
	private String allMessage() {
		String list = "Messagens\n";
		for( String key : messages.keySet() ) {
			list += "Conversa com " + key + "\n";
			LinkedList< Message > message = messages.get(key);
			for( int i = 0 ; i < message.size() ; ++i ) {
				list += message.get(i) + "\n";
			}
		}
		return list;
	}
}
