package iResearcher;

import java.util.TreeSet;

public class Community {

	private String name;
	private String description;
	private TreeSet< String > associates;
	
	public Community( String name, String description ) {
		setName(name);
		setDescription(description);
		associates = new TreeSet<String>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public TreeSet<String> getAssociates() {
		return associates;
	}

	public void setAssociates(TreeSet<String> associates) {
		this.associates = associates;
	}

	public void addAssociate( String nick ) {
		associates.add(nick);
	}
	
	public void removeAssociate( String nick ) {
		associates.remove(nick);
	}
	
}
