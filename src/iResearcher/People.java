package iResearcher;

public class People {

	protected String name;
	protected int age;
	
	public People( String name, int age ) {
		setAge(age);
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		
		return name + "\n";
	}
}
